package time;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TimeTest {
	
	//Test for Lab 3- Part 1 fixed 
	@Test
	public void testGetTotalMilliseconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0B");
		fail("Invalid number of milliseconds");
	}
	
	@Test 
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
		
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}

	
	//Test for Lab 3 - Part 1 fail 
//	@Test
//	public void testGetTotalMilliseconds() {
//		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
//		fail("Invalid number of milliseconds");
//	}
	

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		//what is the expected value here
		assertTrue("The time provided does not match the result.", totalSeconds == 3661); //compare value we got to the one that's expected
	
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("A1:01:99");
		
		//what is the expected value here
		assertFalse("The time provided does not match the result.", totalSeconds == 3719); //compare value we got to the one that's expected
	
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		
		//what is the expected value here
		assertTrue("The time provided does not match the result.", totalSeconds == 3719); //compare value we got to the one that's expected
	
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		
		//what is the expected value here
		assertFalse("The time provided does not match the result.", totalSeconds == 3720); //compare value we got to the one that's expected
	
	}

}
