package phones;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SmartPhoneTest {

	@Test
	public void testGetFormattedPriceRegular() {
		
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.12, 12.00);
		assertTrue("Incorrect price format", iphone.getFormattedPrice().equals("$1,500.12"));
	}
	
	@Test (expected = AssertionError.class)
	public void testGetFormattedPriceException() {
		
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.122, 12.00);
		assertFalse("Incorrect price format", iphone.getFormattedPrice().equals("$1,500.122"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 888.0, 12.00);
		assertTrue("Incorrect price format", iphone.getFormattedPrice().equals("$888"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 888.10, 12.00);
		assertFalse("Incorrect price format", iphone.getFormattedPrice().equals("$888.10"));
	}

	
	//JUnit Test for Set Version Number
	@Test
	public void testSetVersionRegular() throws VersionNumberException{
		
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.12, 3.00);
		iphone.setVersion(3.00);
		assertTrue("Valid Version Number", iphone.getVersion() == 3.00);
	}
	
	@Test (expected = AssertionError.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.12, 88);
		assertFalse("Invalid Version Number", iphone.getVersion() == 88);
	}
	
	@Test 
	public void testSetVersionBoundaryIn() throws VersionNumberException{
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.12, 3.99);
		iphone.setVersion(3.99);
		assertTrue("Valid Version Number", iphone.getVersion() == 3.99);
	}
	
	@Test (expected = AssertionError.class)
	public void testSetVersionBoundaryOut() throws VersionNumberException{
		SmartPhone iphone = new SmartPhone("Apple iPhone X", 1500.12, 4.10);
		assertFalse("Invalid Version Number", iphone.getVersion() == 4.10);

	}

}
